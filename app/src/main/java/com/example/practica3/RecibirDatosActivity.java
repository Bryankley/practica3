package com.example.practica3;
import android.os.Bundle;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
public class RecibirDatosActivity extends AppCompatActivity {
  TextView nombre, apellido, edad, cedula;
  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_recibir_datos);
    nombre = findViewById(R.id.nombre);
    apellido = findViewById(R.id.apellido);
    edad = findViewById(R.id.edad);
    cedula = findViewById(R.id.cedula);
    Bundle bundle = this.getIntent().getExtras();
    nombre.setText(bundle.getString("nombre"));
    apellido.setText(bundle.getString("apellido"));
    edad.setText(bundle.getString("edad"));
    cedula.setText(bundle.getString("cedula"));
  }
}

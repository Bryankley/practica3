package com.example.practica3;
import android.os.Bundle;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
public class RecibirParametroActivity extends AppCompatActivity {
  TextView texto;
  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_recibir_parametro);
    texto = findViewById(R.id.recibirparametro);
    Bundle bundle = this.getIntent().getExtras();
    texto.setText(bundle.getString("dato"));
  }
}

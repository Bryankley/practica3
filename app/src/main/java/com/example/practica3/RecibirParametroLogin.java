package com.example.practica3;
import android.os.Bundle;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
public class RecibirParametroLogin extends AppCompatActivity {
  TextView texto1, texto2;
  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_recibir_parametro_login);
    texto1 = findViewById(R.id.recibirparametro1);
    texto2 = findViewById(R.id.recibirparametro2);
    Bundle bundle = this.getIntent().getExtras();
    texto1.setText(bundle.getString("dato1"));
    texto2.setText(bundle.getString("dato2"));
  }
}

package com.example.practica3;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import androidx.appcompat.app.AppCompatActivity;
public class PasarParametroActivity extends AppCompatActivity {
  EditText cajatexto;
  Button enviarparametro;
  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_pasar_parametro);
    cajatexto = findViewById(R.id.textoparametro);
    enviarparametro = findViewById(R.id.pasarparametro);
    enviarparametro.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        Intent intent = new Intent(PasarParametroActivity.this, RecibirParametroActivity.class);
        Bundle bundle = new Bundle();
        bundle.putString("dato", cajatexto.getText().toString());
        intent.putExtras(bundle);
        startActivity(intent);
      }
    });
  }
}

package com.example.practica3;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import androidx.appcompat.app.AppCompatActivity;
public class LoginActivity extends AppCompatActivity {
  EditText Usuario;
  EditText Contraseña;
  Button Boton;
  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_login);
    Usuario = findViewById(R.id.usuario);
    Contraseña = findViewById(R.id.password);
    Boton = findViewById(R.id.btnparametro);
    Boton.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        Intent intent = new Intent(LoginActivity.this, RecibirParametroLogin.class);
        Bundle bundle = new Bundle();
        bundle.putString("dato1", Usuario.getText().toString());
        bundle.putString("dato2", Contraseña.getText().toString());
        intent.putExtras(bundle);
        startActivity(intent);
      }
    });
  }
}

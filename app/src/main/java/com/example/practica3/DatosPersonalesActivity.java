package com.example.practica3;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import androidx.appcompat.app.AppCompatActivity;
public class DatosPersonalesActivity extends AppCompatActivity {
  EditText nombre, apellido, edad, cedula;
  Button registrar;
  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_datos_personales);
    nombre = findViewById(R.id.nombre);
    apellido = findViewById(R.id.apellido);
    edad = findViewById(R.id.edad);
    cedula = findViewById(R.id.cedula);
    registrar = findViewById(R.id.registrar);
    registrar.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        Intent intent = new Intent(DatosPersonalesActivity.this, RecibirDatosActivity.class);
        Bundle bundle = new Bundle();
        //  bundle.putString("dato", cajatexto.getText().toString());
        bundle.putString("nombre", nombre.getText().toString());
        bundle.putString("apellido", apellido.getText().toString());
        bundle.putString("edad", edad.getText().toString());
        bundle.putString("cedula", cedula.getText().toString());
        intent.putExtras(bundle);
        startActivity(intent);
      }
    });
  }
  public boolean onCreateOptionsMenu(Menu menu) {
    MenuInflater inflater = getMenuInflater();
    inflater.inflate(R.menu.main_menu, menu);
    return true;
  }
  public boolean onOptionsItemSelected(MenuItem item) {
    switch (item.getItemId()) {
      case R.id.opcionLogin:
        startActivity(new Intent(DatosPersonalesActivity.this, LoginActivity.class));
        break;
      case R.id.opcionRegistrar:
        startActivity(new Intent(DatosPersonalesActivity.this, DatosPersonalesActivity.class));
        break;
    }
    return true;
  }
}

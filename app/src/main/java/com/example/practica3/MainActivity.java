package com.example.practica3;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;

import androidx.appcompat.app.AppCompatActivity;
public class MainActivity extends AppCompatActivity {
  Button buttonlogin, buttonguardar, buttonbuscar, buttonpasarparametro, buttondatos;
  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_main);
    buttonlogin = findViewById(R.id.login);
    buttonguardar = findViewById(R.id.guardar);
    buttonbuscar = findViewById(R.id.buscar);
    buttonpasarparametro = findViewById(R.id.parametro);
    buttondatos = findViewById(R.id.datos);
    buttonlogin.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        startActivity(new Intent(MainActivity.this, LoginActivity.class));
      }
    });
    buttonguardar.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        startActivity(new Intent(MainActivity.this, GuardarActivity.class));
      }
    });
    buttonbuscar.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        startActivity(new Intent(MainActivity.this, BuscarActivity.class));
      }
    });
    buttonpasarparametro.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        startActivity(new Intent(MainActivity.this, PasarParametroActivity.class));
      }
    });
    buttondatos.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        startActivity(new Intent(MainActivity.this, DatosPersonalesActivity.class));
      }
    });
  }
  public boolean onCreateOptionsMenu(Menu menu) {
    MenuInflater inflater = getMenuInflater();
    inflater.inflate(R.menu.main, menu);
    return true;
  }
  public boolean onOptionsItemSelected(MenuItem item) {
    switch (item.getItemId()) {
      case R.id.opcionLogin:
        startActivity(new Intent(MainActivity.this, LoginActivity.class));
        break;
      case R.id.opcionRegistrar:
        startActivity(new Intent(MainActivity.this, DatosPersonalesActivity.class));
        break;
    }
    return true;
  }
}
